#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

CONFIG_FILE="/etc/nixos/configuration.nix"

insert_before_last_brace() {
    sed -i "/^}$/i $1" "$CONFIG_FILE"
}

echo "Thank you for using Gosh-Its-Arch post NixOS install script"

ask_rebuild() {
    read -p "Would you like to perform a nixos-rebuild switch now? (Y/n): " confirm
    [[ $confirm == "Y" || $confirm == "y" || $confirm == "" ]] && nixos-rebuild switch
}

enable_flatpaks() {
    insert_before_last_brace "services.flatpak.enable = true;"
}

enable_kvm() {
    insert_before_last_brace "boot.kernelModules = [ \"kvm-amd\" \"kvm-intel\" ];"
    insert_before_last_brace "virtualisation.libvirtd.enable = true;"
    read -p "Which user should be added to the libvirtd group? " user_name
    usermod -aG libvirtd $user_name
}

disable_kvm() {
    sed -i "/boot.kernelModules = \[ \"kvm-amd\" \"kvm-intel\" \];/d" "$CONFIG_FILE"
    sed -i "/virtualisation.libvirtd.enable = true;/d" "$CONFIG_FILE"
}

set_dns() {
    read -p "Enter the custom DNS IP: " dns_ip
    insert_before_last_brace "environment.etc = { \"resolv.conf\".text = \"nameserver $dns_ip\"; };"
}

disable_dns() {
    sed -i "/environment.etc = { \"resolv.conf\".text = \"nameserver .*\"; };/d" "$CONFIG_FILE"
}

enable_docker() {
    insert_before_last_brace "virtualisation.docker.enable = true;"
    sed -i "/virtualisation.podman.enable = true;/d" "$CONFIG_FILE"
}

enable_podman() {
    insert_before_last_brace "virtualisation.podman.enable = true;"
    sed -i "/virtualisation.docker.enable = true;/d" "$CONFIG_FILE"
}

disable_docker_podman() {
    sed -i "/virtualisation.docker.enable = true;/d" "$CONFIG_FILE"
    sed -i "/virtualisation.podman.enable = true;/d" "$CONFIG_FILE"
}

enable_vbox() {
    read -p "Which user should have access to VirtualBox? " vbox_user
    insert_before_last_brace "virtualisation.virtualbox.host.enable = true;"
    insert_before_last_brace "users.extraGroups.vboxusers.members = [ \"$vbox_user\" ];"
    insert_before_last_brace "virtualisation.virtualbox.host.enableExtensionPack = true;"
    insert_before_last_brace "virtualisation.virtualbox.guest.enable = true;"
    insert_before_last_brace "virtualisation.virtualbox.guest.x11 = true;"
}

disable_vbox() {
    sed -i "/virtualisation.virtualbox.host.enable = true;/d" "$CONFIG_FILE"
    sed -i "/users.extraGroups.vboxusers.members = \[ \".*\" \];/d" "$CONFIG_FILE"
    sed -i "/virtualisation.virtualbox.host.enableExtensionPack = true;/d" "$CONFIG_FILE"
    sed -i "/virtualisation.virtualbox.guest.enable = true;/d" "$CONFIG_FILE"
    sed -i "/virtualisation.virtualbox.guest.x11 = true;/d" "$CONFIG_FILE"
}

enable_flathub() {
    flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
}

add_libvirtd_user() {
    read -p "Which user should be added to the libvirtd group? " user_name
    usermod -aG libvirtd $user_name
}

garbage_collection() {
    echo "1) Delete unreachable store objects"
    echo "2) Delete all old generations of all profiles (Warning: This option will erase old configurations)"
    read -p "Choose an option: " gc_choice

    case $gc_choice in
        1) nix-collect-garbage; ask_rebuild;;
        2)
            read -p "Are you sure? (Y/n): " gc_confirm
            [[ $gc_confirm == "Y" || $gc_confirm == "y" ]] && nix-collect-garbage -d && ask_rebuild
            ;;
    esac
}

show_menu() {
    clear
    echo "1) Enable Flatpaks"
    echo "2) Enable KVM"
    echo "3) Disable KVM"
    echo "4) Set Custom DNS Resolver"
    echo "5) Disable Custom DNS Resolver"
    echo "6) Enable Docker"
    echo "7) Enable Podman"
    echo "8) Disable Docker/Podman"
    echo "9) Enable VirtualBox (Non-Free)"
    echo "10) Disable VirtualBox (Non-Free)"
    echo "11) Enable Flathub"
    echo "12) Add User to libvirtd group"
    echo "13) Garbage Collection"
    echo "0) Exit"
    read -p "Enter your choice: " choice

    case $choice in
        1) enable_flatpaks; ask_rebuild;;
        2) enable_kvm; ask_rebuild;;
        3) disable_kvm; ask_rebuild;;
        4) set_dns; ask_rebuild;;
        5) disable_dns; ask_rebuild;;
        6) enable_docker; ask_rebuild;;
        7) enable_podman; ask_rebuild;;
        8) disable_docker_podman; ask_rebuild;;
        9) enable_vbox; ask_rebuild;;
        10) disable_vbox; ask_rebuild;;
        11) enable_flathub; ask_rebuild;;
        12) add_libvirtd_user; ask_rebuild;;
        13) garbage_collection;;
        0) exit 0;;
        *) echo "Invalid option";;
    esac
    show_menu
}

show_menu
